package br.com.fatec.connectlayer.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.com.fatec.connectlayer.process.Message;

public class LayerTableModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final int COLUMN_LETTER = 0;
	public static final String COLLUMN_LETTER_NAME = "Letra";
	public static final int COLUMN_BINARY = 1;
	public static final String COLLUMN_BINARY_NAME = "Binário";

	private String[] columnNames = { COLLUMN_LETTER_NAME, COLLUMN_BINARY_NAME };
	private List<Message> data;

	public LayerTableModel() {
		this.data = new ArrayList<Message>(10);
	}

	public int getRowCount() {
		return data.size();
	}

	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}

	public Object getValueAt(int rowIndex, int columnIndex) {

		LayerModel layerModel = (LayerModel) data.get(rowIndex);

		if (layerModel != null) {

			switch (columnIndex) {

			case COLUMN_LETTER:
				return layerModel.getLetter();
			case COLUMN_BINARY:
				return layerModel.getBinary();
			default:
				throw new IndexOutOfBoundsException("ColumnIndex out of bounds.");

			}
		}
		return null;
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

		LayerModel layerModel = (LayerModel) data.get(rowIndex);

		if (layerModel != null) {

			switch (columnIndex) {
			case COLUMN_LETTER:
				layerModel.setLetter((String) aValue);
				break;

			case COLUMN_BINARY:
				layerModel.setBinary((String) aValue);

			default:
				throw new IndexOutOfBoundsException("ColumnIndex out of bounds");
			}
		}
		fireTableCellUpdated(rowIndex, columnIndex);
	}

	public LayerModel getLayerModel(int rowIndex) {
		return (LayerModel) data.get(rowIndex);
	}

	public void addLayer(LayerModel layerModel) {
		data.add(layerModel);
		int lastIndex = getRowCount() - 1;
		fireTableRowsInserted(lastIndex, lastIndex);
	}

	public void removeLayer(int rowIndex) {
		data.remove(rowIndex);
		fireTableRowsDeleted(rowIndex, rowIndex);
	}

	public void clear() {
		data.clear();
		fireTableDataChanged();
	}

	public List<Message> getData() {
		return this.data;
	}

}
