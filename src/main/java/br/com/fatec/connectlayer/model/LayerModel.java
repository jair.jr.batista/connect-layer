package br.com.fatec.connectlayer.model;

import java.io.Serializable;

import br.com.fatec.connectlayer.process.Message;

public class LayerModel implements Serializable, Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String letter;
	private String binary;

	public LayerModel() {

	}

	public LayerModel(String letter, String binary) {
		this.letter = letter;
		this.binary = binary;
	}

	public String getLetter() {
		return letter;
	}

	public void setLetter(String letter) {
		this.letter = letter;
	}

	public String getBinary() {
		return binary;
	}

	public void setBinary(String bin) {
		bin = ("00000000" + bin);
		this.binary = bin.substring(bin.length()-8, bin.length());
	}

	@Override
	public String toString() {
		return "LayerModel [letter=" + letter + ", binary=" + binary + "]";
	}

	@Override
	public String getPayload() {
		
		return this.letter;
	}
	
	

}
