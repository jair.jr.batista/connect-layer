package br.com.fatec.connectlayer.process;

public interface Message {
	
	String getPayload();

}
