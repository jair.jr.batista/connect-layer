package br.com.fatec.connectlayer.process;

import java.util.List;

import br.com.fatec.connectlayer.network.ClientContainer;
import io.netty.channel.Channel;

public class ProcessThread extends Thread {

	private List<Message> dataProcess;
	private ClientContainer clientContainer = ClientContainer.getInstance();

	@Override
	public void run() {

		Channel channel = clientContainer.getChannel();

		if (dataProcess != null) {

			for (Message message : dataProcess) {

				System.out.println(message.getPayload());
//				channel.writeAndFlush(Unpooled.copiedBuffer(message.getPayload(), CharsetUtil.UTF_8))
//						.syncUninterruptibly();
				channel.write(message.getPayload() + "\n");
				channel.flush();
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		} else {
			throw new RuntimeException(
					"A lista de dados nunca pode ser nula, use o metodo [start( List<T> dataProcess )]");
		}

	}

	public void start(List<Message> dataProcess) {
		this.dataProcess = dataProcess;
		this.run();
	}

}
