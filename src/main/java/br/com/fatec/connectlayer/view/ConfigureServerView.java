package br.com.fatec.connectlayer.view;

import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import br.com.fatec.connectlayer.network.ClientContainer;
import br.com.fatec.connectlayer.network.DefaultClientAdapterInitializer;
import javax.swing.JSpinner;

public class ConfigureServerView extends JDialog implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	ClientContainer clientContainer = ClientContainer.getInstance();

	private JPanel contentPane;
	private JTextField txtIpAddress;
	private JTextField txtPorta;
	private JButton btnCancelar;
	private JButton btnConectar;

	private static ConfigureServerView INSTANCE;
	private JLabel lblDelay;
	private JSpinner spnDelay;

	public static ConfigureServerView getInstance() {

		if (INSTANCE == null) {
			INSTANCE = new ConfigureServerView();
		}

		return INSTANCE;
	}

	/**
	 * Create the frame.
	 */
	private ConfigureServerView() {
		Point center = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setTitle("Configuração do Servidor");
		setResizable(false);
		int windowWidth = 280;
		int windowHeight = 210;
		setBounds(center.x - windowWidth / 2, center.y - windowHeight / 2, windowWidth, windowHeight);
		setModal(true);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblIp = new JLabel("Endereço IP:");
		lblIp.setBounds(26, 37, 81, 16);
		contentPane.add(lblIp);

		JLabel lblPorta = new JLabel("Porta:");
		lblPorta.setBounds(26, 72, 61, 16);
		contentPane.add(lblPorta);

		txtIpAddress = new JTextField();
		txtIpAddress.setText("127.0.0.1");
		txtIpAddress.setBounds(106, 32, 150, 26);
		contentPane.add(txtIpAddress);
		txtIpAddress.setColumns(10);

		txtPorta = new JTextField();
		txtPorta.setText("7879");
		txtPorta.setBounds(106, 65, 150, 26);
		contentPane.add(txtPorta);
		txtPorta.setColumns(10);

		btnConectar = new JButton(clientContainer.isConnected() ? "Desconectar" : "Conectar");
		btnConectar.setBounds(14, 142, 130, 29);
		btnConectar.addActionListener(this);
		contentPane.add(btnConectar);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(this);
		btnCancelar.setBounds(144, 142, 130, 29);
		contentPane.add(btnCancelar);

		lblDelay = new JLabel("Delay:");
		lblDelay.setBounds(26, 108, 61, 16);
		contentPane.add(lblDelay);

		spnDelay = new JSpinner();
		spnDelay.setBounds(106, 103, 53, 26);
		spnDelay.setValue(1);
		contentPane.add(spnDelay);

		JLabel lblSegundos = new JLabel("segundo(s)");
		lblSegundos.setBounds(171, 108, 85, 16);
		contentPane.add(lblSegundos);
	}
	
	@Override
	public void show() {
		btnConectar.setText(clientContainer.isConnected() ? "Desconectar" : "Conectar");
		super.show();
	}

	@Override
	public void setVisible(boolean b) {

		if (!isVisible()) {
			super.setVisible(b);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		Integer delay = (Integer) spnDelay.getValue();

		if (delay <= 0) {
			delay = 1;
			spnDelay.setValue(delay);
		}

		if (e.getSource().equals(btnConectar)) {

			if (clientContainer.isConnected()) {
				clientContainer.close();
			} else {

				clientContainer
					.configureIpAddress(txtIpAddress.getText())
					.configurePort(Integer.parseInt(txtPorta.getText()))
					.configureDelay(delay)
					.configureHandler(new DefaultClientAdapterInitializer())
					.start();
			}

		}

		this.dispose();

	}
}
