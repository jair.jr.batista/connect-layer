package br.com.fatec.connectlayer.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;

import br.com.fatec.connectlayer.model.LayerModel;
import br.com.fatec.connectlayer.model.LayerTableModel;
import br.com.fatec.connectlayer.network.ClientContainer;
import br.com.fatec.connectlayer.network.OnConnect;
import io.netty.channel.Channel;

public class MainView extends JFrame implements OnConnect {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ClientContainer clientContainer;

	private JPanel contentPane;
	private JButton btnEnviar;
	private JButton btnConfigurar;
	private JTable dataTableProcess;
	private JTextField bit0TextField;
	private JTextField bit1TextField;
	private JTextField bit2TextField;
	private JTextField bit3TextField;
	private JTextField bit4TextField;
	private JTextField bit5TextField;
	private JTextField bit6TextField;
	private JTextField bit7TextField;
	private JLabel led0;
	private JLabel led1;
	private JLabel led2;
	private JLabel led3;
	private JLabel led4;
	private JLabel led5;
	private JLabel led6;
	private JLabel led7;
	private JTextField letterTextField;
	private JTextField hexCodeTextField;
	private JTextArea targetMessageTextArea;
	private JTextArea targetBinaryMessageTextArea;

	private JTextArea sourceMessageTextArea;
	private JTextArea sourceBinaryMessageTextArea;

	private LayerTableModel layerDataModel = new LayerTableModel();
	
	private JLabel lblConnectIpAddress;
	private JLabel lblConnectPortAddress;
	private JLabel lblConnectDelay;
	private JLabel lblConnectStatus;
	private JButton btnReset;

	private ActionListener sendActionListener = new ActionListener() {

		public void actionPerformed(ActionEvent e) {

			new Thread(new Runnable() {

				@Override
				public void run() {
					try {

						btnEnviar.setEnabled(false);
						btnConfigurar.setEnabled(false);
						btnReset.setEnabled(false);

						layerDataModel.clear();
						
						
						String sourceMessage = sourceMessageTextArea.getText();

						targetBinaryMessageTextArea.setText("");
						targetMessageTextArea.setText("");

						for (int i = 0; i < sourceMessage.length(); i++) {

							LayerModel layerModel = new LayerModel();
							layerModel.setLetter( String.valueOf(sourceMessage.charAt(i)));
							layerModel.setBinary(Integer.toBinaryString(Character.codePointAt(sourceMessage, i)));

							String binary = layerModel.getBinary();

							String oldBin = targetBinaryMessageTextArea.getText();
							targetBinaryMessageTextArea.setText((oldBin + " " + binary).trim());

							String oldMessage = targetMessageTextArea.getText();
							targetMessageTextArea.setText(oldMessage + layerModel.getLetter());

							bit0TextField.setText(binary.substring(7, 8));
							led0.setEnabled(bit0TextField.getText().equals("1") ? true : false);

							bit1TextField.setText(binary.substring(6, 7));
							led1.setEnabled(bit1TextField.getText().equals("1") ? true : false);

							bit2TextField.setText(binary.substring(5, 6));
							led2.setEnabled(bit2TextField.getText().equals("1") ? true : false);

							bit3TextField.setText(binary.substring(4, 5));
							led3.setEnabled(bit3TextField.getText().equals("1") ? true : false);

							bit4TextField.setText(binary.substring(3, 4));
							led4.setEnabled(bit4TextField.getText().equals("1") ? true : false);

							bit5TextField.setText(binary.substring(2, 3));
							led5.setEnabled(bit5TextField.getText().equals("1") ? true : false);

							bit6TextField.setText(binary.substring(1, 2));
							led6.setEnabled(bit6TextField.getText().equals("1") ? true : false);

							bit7TextField.setText(binary.substring(0, 1));
							led7.setEnabled(bit7TextField.getText().equals("1") ? true : false);

							letterTextField.setText(layerModel.getLetter());
							hexCodeTextField.setText(
									"0x" + Integer.toHexString(Character.codePointAt(layerModel.getLetter(), 0)));

							layerDataModel.addLayer(layerModel);

							if (clientContainer.isConnected()) {
								clientContainer.send(layerModel.getLetter());
							}

							Thread.sleep(clientContainer.getDelay());

						}

						// processThread.start(layerDataModel.getData());

					} catch (UnsupportedEncodingException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					btnEnviar.setEnabled(true);
					btnConfigurar.setEnabled(true);
					btnReset.setEnabled(true);
				}
			}).start();

		}
	};


	/**
	 * Create the frame.
	 */
	public MainView() {

		Point center = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();

		clientContainer = ClientContainer.getInstance();
		clientContainer.configureConnectionListener(this);

		setResizable(false);
		setTitle("Connect Layer:::v1");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		int windowWidth = 1024;
		int windowHeight = 530;
		setBounds(center.x - windowWidth / 2, center.y - windowHeight / 2, windowWidth, windowHeight);
		// setBounds(100, 100, 875, 505);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);

		setContentPane(contentPane);

		JTabbedPane sourceTabbedPane = new JTabbedPane(JTabbedPane.TOP);
		sourceTabbedPane.setToolTipText("");
		sourceTabbedPane.setBounds(6, 34, 280, 468);
		contentPane.add(sourceTabbedPane);

		JPanel sourcePanel = new JPanel();
		sourceTabbedPane.addTab("Origem", null, sourcePanel, null);
		sourceTabbedPane.setEnabledAt(0, true);
		sourcePanel.setLayout(null);

		JLabel lblMensagem = new JLabel("Mensagem");
		lblMensagem.setBounds(10, 10, 240, 20);
		sourcePanel.add(lblMensagem);

		JLabel lblBinario = new JLabel("Binário");
		lblBinario.setBounds(10, 180, 240, 20);
		sourcePanel.add(lblBinario);

		btnEnviar = new JButton("Processar");
		btnEnviar.addActionListener(sendActionListener);
		btnEnviar.setBounds(10, 343, 240, 35);
		sourcePanel.add(btnEnviar);

		JScrollPane sourceMessageScrollPane = new JScrollPane();
		sourceMessageScrollPane.setViewportBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		sourceMessageScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		sourceMessageScrollPane.setBounds(10, 30, 240, 140);
		sourcePanel.add(sourceMessageScrollPane);

		sourceMessageTextArea = new JTextArea();
		sourceMessageTextArea.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				
				String sourceMessage = sourceMessageTextArea.getText();
				sourceBinaryMessageTextArea.setText("");
				
				for (int i = 0; i < sourceMessage.length(); i++) {
					
					int codePointAt = Character.codePointAt(sourceMessage, i);
					String binary = "00000000" + Integer.toBinaryString(codePointAt);
					binary = binary.substring(binary.length() - 8, binary.length());
					sourceBinaryMessageTextArea.append(binary + " ");
					
				}
				
			}
		});
		sourceMessageTextArea.setWrapStyleWord(true);
		sourceMessageTextArea.setLineWrap(true);
		sourceMessageTextArea.setBackground(new Color(144, 238, 144));
		sourceMessageScrollPane.setViewportView(sourceMessageTextArea);

		JScrollPane sourceBinaryMessageScrollPane = new JScrollPane();
		sourceBinaryMessageScrollPane.setViewportBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		sourceBinaryMessageScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		sourceBinaryMessageScrollPane.setBounds(10, 200, 240, 140);
		sourcePanel.add(sourceBinaryMessageScrollPane);

		sourceBinaryMessageTextArea = new JTextArea();
		sourceBinaryMessageTextArea.setWrapStyleWord(true);
		sourceBinaryMessageTextArea.setEditable(false);
		sourceBinaryMessageTextArea.setLineWrap(true);
		sourceBinaryMessageTextArea.setBackground(new Color(144, 238, 144));
		sourceBinaryMessageScrollPane.setViewportView(sourceBinaryMessageTextArea);
		
		btnReset = new JButton("Reset");
		btnReset.setBounds(10, 381, 240, 35);
		btnReset.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				layerDataModel.clear();
				
				sourceBinaryMessageTextArea.setText("");
				sourceMessageTextArea.setText("");
				targetBinaryMessageTextArea.setText("");
				targetMessageTextArea.setText("");
				
				bit0TextField.setText("0");
				led0.setEnabled(false);

				bit1TextField.setText("0");
				led1.setEnabled(false);

				bit2TextField.setText("0");
				led2.setEnabled(false);
				
				bit3TextField.setText("0");
				led3.setEnabled(false);

				bit4TextField.setText("0");
				led4.setEnabled(false);

				bit5TextField.setText("0");
				led5.setEnabled(false);

				bit6TextField.setText("0");
				led6.setEnabled(false);

				bit7TextField.setText("0");
				led7.setEnabled(false);
				
				letterTextField.setText("");
				hexCodeTextField.setText("0x00");				
				
			}
		});
		sourcePanel.add(btnReset);

		JTabbedPane processTabbedPane = new JTabbedPane(JTabbedPane.TOP);
		processTabbedPane.setToolTipText("");
		processTabbedPane.setBounds(283, 34, 457, 468);
		contentPane.add(processTabbedPane);

		JPanel processPanel = new JPanel();
		processTabbedPane.addTab("Processamento", null, processPanel, null);
		processTabbedPane.setEnabledAt(0, true);
		processPanel.setLayout(null);

		JLabel lblProcessamento = new JLabel("Dados");
		lblProcessamento.setBounds(10, 10, 240, 20);
		processPanel.add(lblProcessamento);

		JScrollPane dataTableScrollPane = new JScrollPane();
		dataTableScrollPane.setEnabled(false);
		dataTableScrollPane.setViewportBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		dataTableScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		dataTableScrollPane.setBounds(10, 30, 170, 326);
		processPanel.add(dataTableScrollPane);

		dataTableProcess = new JTable(layerDataModel);
		dataTableProcess.setRowSelectionAllowed(false);
		dataTableScrollPane.setViewportView(dataTableProcess);

		bit0TextField = new JTextField();
		bit0TextField.setEditable(false);
		bit0TextField.setHorizontalAlignment(SwingConstants.CENTER);
		bit0TextField.setText("0");
		bit0TextField.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		bit0TextField.setBounds(370, 371, 60, 45);
		processPanel.add(bit0TextField);
		bit0TextField.setColumns(10);

		bit1TextField = new JTextField();
		bit1TextField.setEditable(false);
		bit1TextField.setHorizontalAlignment(SwingConstants.CENTER);
		bit1TextField.setText("0");
		bit1TextField.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		bit1TextField.setColumns(10);
		bit1TextField.setBounds(310, 371, 60, 45);
		processPanel.add(bit1TextField);

		bit2TextField = new JTextField();
		bit2TextField.setEditable(false);
		bit2TextField.setHorizontalAlignment(SwingConstants.CENTER);
		bit2TextField.setText("0");
		bit2TextField.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		bit2TextField.setColumns(10);
		bit2TextField.setBounds(251, 371, 60, 45);
		processPanel.add(bit2TextField);

		bit3TextField = new JTextField();
		bit3TextField.setEditable(false);
		bit3TextField.setHorizontalAlignment(SwingConstants.CENTER);
		bit3TextField.setText("0");
		bit3TextField.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		bit3TextField.setColumns(10);
		bit3TextField.setBounds(190, 371, 60, 45);
		processPanel.add(bit3TextField);

		bit4TextField = new JTextField();
		bit4TextField.setEditable(false);
		bit4TextField.setHorizontalAlignment(SwingConstants.CENTER);
		bit4TextField.setText("0");
		bit4TextField.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		bit4TextField.setColumns(10);
		bit4TextField.setBounds(370, 155, 60, 45);
		processPanel.add(bit4TextField);

		bit5TextField = new JTextField();
		bit5TextField.setEditable(false);
		bit5TextField.setHorizontalAlignment(SwingConstants.CENTER);
		bit5TextField.setText("0");
		bit5TextField.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		bit5TextField.setColumns(10);
		bit5TextField.setBounds(310, 155, 60, 45);
		processPanel.add(bit5TextField);

		bit6TextField = new JTextField();
		bit6TextField.setEditable(false);
		bit6TextField.setHorizontalAlignment(SwingConstants.CENTER);
		bit6TextField.setText("0");
		bit6TextField.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		bit6TextField.setColumns(10);
		bit6TextField.setBounds(251, 155, 60, 45);
		processPanel.add(bit6TextField);

		bit7TextField = new JTextField();
		bit7TextField.setEditable(false);
		bit7TextField.setHorizontalAlignment(SwingConstants.CENTER);
		bit7TextField.setText("0");
		bit7TextField.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
		bit7TextField.setColumns(10);
		bit7TextField.setBounds(190, 155, 60, 45);
		processPanel.add(bit7TextField);

		JLabel lblNewLabel = new JLabel("Bit7");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(190, 142, 60, 16);
		processPanel.add(lblNewLabel);

		JLabel lblBit_6 = new JLabel("Bit6");
		lblBit_6.setHorizontalAlignment(SwingConstants.CENTER);
		lblBit_6.setBounds(251, 142, 60, 16);
		processPanel.add(lblBit_6);

		JLabel lblBit_5 = new JLabel("Bit5");
		lblBit_5.setHorizontalAlignment(SwingConstants.CENTER);
		lblBit_5.setBounds(310, 142, 60, 16);
		processPanel.add(lblBit_5);

		JLabel lblBit_4 = new JLabel("Bit4");
		lblBit_4.setHorizontalAlignment(SwingConstants.CENTER);
		lblBit_4.setBounds(370, 142, 60, 16);
		processPanel.add(lblBit_4);

		JLabel lblBit_3 = new JLabel("Bit3");
		lblBit_3.setHorizontalAlignment(SwingConstants.CENTER);
		lblBit_3.setBounds(190, 355, 60, 16);
		processPanel.add(lblBit_3);

		JLabel lblBit_2 = new JLabel("Bit2");
		lblBit_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblBit_2.setBounds(251, 355, 60, 16);
		processPanel.add(lblBit_2);

		JLabel lblBit_1 = new JLabel("Bit1");
		lblBit_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblBit_1.setBounds(310, 355, 60, 16);
		processPanel.add(lblBit_1);

		JLabel lblBit = new JLabel("Bit0");
		lblBit.setHorizontalAlignment(SwingConstants.CENTER);
		lblBit.setBounds(370, 355, 60, 16);
		processPanel.add(lblBit);

		JLabel lblLetra = new JLabel("Letra:");
		lblLetra.setHorizontalAlignment(SwingConstants.CENTER);
		lblLetra.setBounds(10, 361, 61, 16);
		processPanel.add(lblLetra);

		letterTextField = new JTextField();
		letterTextField.setForeground(Color.BLUE);
		letterTextField.setEditable(false);
		letterTextField.setFont(new Font("Lucida Grande", Font.PLAIN, 18));
		letterTextField.setHorizontalAlignment(SwingConstants.CENTER);
		letterTextField.setText("");
		letterTextField.setBounds(10, 376, 60, 40);
		processPanel.add(letterTextField);
		letterTextField.setColumns(10);

		hexCodeTextField = new JTextField();
		hexCodeTextField.setForeground(Color.RED);
		hexCodeTextField.setText("0x00");
		hexCodeTextField.setHorizontalAlignment(SwingConstants.CENTER);
		hexCodeTextField.setFont(new Font("Lucida Grande", Font.PLAIN, 18));
		hexCodeTextField.setEditable(false);
		hexCodeTextField.setColumns(10);
		hexCodeTextField.setBounds(100, 376, 80, 40);
		processPanel.add(hexCodeTextField);

		JLabel lblHex = new JLabel("Hex:");
		lblHex.setHorizontalAlignment(SwingConstants.CENTER);
		lblHex.setBounds(110, 361, 61, 16);
		processPanel.add(lblHex);

		led7 = new JLabel("");
		led7.setIcon(new ImageIcon(getPath() + "/images/light-red-on.png"));
		led7.setHorizontalAlignment(SwingConstants.CENTER);
		led7.setBounds(190, 30, 60, 100);
		led7.setEnabled(false);
		processPanel.add(led7);

		led6 = new JLabel("");
		led6.setIcon(new ImageIcon(getPath() + "/images/light-green-on.png"));
		led6.setHorizontalAlignment(SwingConstants.CENTER);
		led6.setBounds(251, 30, 60, 100);
		led6.setEnabled(false);
		processPanel.add(led6);

		led5 = new JLabel("");
		led5.setIcon(new ImageIcon(getPath() + "/images/light-green-on.png"));
		led5.setHorizontalAlignment(SwingConstants.CENTER);
		led5.setBounds(310, 30, 60, 100);
		led5.setEnabled(false);
		processPanel.add(led5);

		led4 = new JLabel("");
		led4.setIcon(new ImageIcon(getPath() + "/images/light-green-on.png"));
		led4.setHorizontalAlignment(SwingConstants.CENTER);
		led4.setBounds(370, 31, 60, 100);
		led4.setEnabled(false);
		processPanel.add(led4);

		led3 = new JLabel("");
		led3.setIcon(new ImageIcon(getPath() + "/images/light-green-on.png"));
		led3.setHorizontalAlignment(SwingConstants.CENTER);
		led3.setBounds(190, 243, 60, 100);
		led3.setEnabled(false);
		processPanel.add(led3);

		led2 = new JLabel("");
		led2.setIcon(new ImageIcon(getPath() + "/images/light-green-on.png"));
		led2.setHorizontalAlignment(SwingConstants.CENTER);
		led2.setBounds(251, 243, 60, 100);
		led2.setEnabled(false);
		processPanel.add(led2);

		led1 = new JLabel("");
		led1.setIcon(new ImageIcon(getPath() + "/images/light-green-on.png"));
		led1.setHorizontalAlignment(SwingConstants.CENTER);
		led1.setBounds(310, 243, 60, 100);
		led1.setEnabled(false);
		processPanel.add(led1);

		led0 = new JLabel("");
		led0.setIcon(new ImageIcon(getPath() + "/images/light-blue-on.png"));
		led0.setHorizontalAlignment(SwingConstants.CENTER);
		led0.setBounds(370, 243, 60, 100);
		led0.setEnabled(false);
		processPanel.add(led0);

		JTabbedPane targetTabbedPane = new JTabbedPane(JTabbedPane.TOP);
		targetTabbedPane.setToolTipText("");
		targetTabbedPane.setBounds(738, 34, 280, 468);
		contentPane.add(targetTabbedPane);

		JPanel targetPanel = new JPanel();
		targetTabbedPane.addTab("Destino", null, targetPanel, null);
		targetPanel.setLayout(null);

		JLabel lblTargetBinaryMessage = new JLabel("Binário");
		lblTargetBinaryMessage.setBounds(10, 10, 240, 16);
		targetPanel.add(lblTargetBinaryMessage);

		JScrollPane targetBinaryMessageScrollPane = new JScrollPane();
		targetBinaryMessageScrollPane.setBounds(10, 30, 240, 140);
		targetBinaryMessageScrollPane.setViewportBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		targetBinaryMessageScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		targetPanel.add(targetBinaryMessageScrollPane);

		targetBinaryMessageTextArea = new JTextArea();
		targetBinaryMessageTextArea.setWrapStyleWord(true);
		targetBinaryMessageTextArea.setLineWrap(true);
		targetBinaryMessageTextArea.setEditable(false);
		targetBinaryMessageTextArea.setBackground(SystemColor.textHighlight);
		targetBinaryMessageScrollPane.setViewportView(targetBinaryMessageTextArea);

		JLabel lblTargetMessage = new JLabel("Mensagem");
		lblTargetMessage.setBounds(10, 180, 240, 20);
		targetPanel.add(lblTargetMessage);

		JScrollPane targetMessageScrollPane = new JScrollPane();
		targetMessageScrollPane.setViewportBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		targetMessageScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		targetMessageScrollPane.setBounds(10, 200, 240, 140);
		targetPanel.add(targetMessageScrollPane);

		targetMessageTextArea = new JTextArea();
		targetMessageTextArea.setLocation(18, 0);
		targetMessageTextArea.setWrapStyleWord(true);
		targetMessageTextArea.setLineWrap(true);
		targetMessageTextArea.setEditable(false);
		targetMessageTextArea.setBackground(SystemColor.textHighlight);
		targetMessageScrollPane.setViewportView(targetMessageTextArea);

		JPanel panel = new JPanel();
		panel.setBounds(10, 344, 240, 72);
		targetPanel.add(panel);
		panel.setLayout(null);

		JLabel lblIp = new JLabel("IP:");
		lblIp.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		lblIp.setBounds(6, 6, 15, 16);
		panel.add(lblIp);

		lblConnectIpAddress = new JLabel("127.0.0.1");
		lblConnectIpAddress.setBounds(33, 6, 95, 16);
		panel.add(lblConnectIpAddress);

		JLabel lblPort = new JLabel("Porta:");
		lblPort.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		lblPort.setBounds(128, 6, 43, 16);
		panel.add(lblPort);

		lblConnectPortAddress = new JLabel("7879");
		lblConnectPortAddress.setBounds(176, 6, 58, 16);
		panel.add(lblConnectPortAddress);
		lblConnectPortAddress.setHorizontalAlignment(SwingConstants.CENTER);

		JLabel lblDelay = new JLabel("Delay:");
		lblDelay.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		lblDelay.setBounds(6, 27, 53, 16);
		panel.add(lblDelay);

		lblConnectDelay = new JLabel("5 segundo(s)");
		lblConnectDelay.setBounds(66, 27, 101, 16);
		panel.add(lblConnectDelay);

		JLabel lblStatus = new JLabel("Status:");
		lblStatus.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		lblStatus.setBounds(6, 51, 58, 16);
		panel.add(lblStatus);

		lblConnectStatus = new JLabel("Desconectado");
		lblConnectStatus.setBounds(66, 51, 168, 16);
		panel.add(lblConnectStatus);
		lblConnectStatus.setForeground(Color.RED);

		btnConfigurar = new JButton("...");
		btnConfigurar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showConfigureView();
			}
		});
		btnConfigurar.setBounds(181, 21, 49, 29);
		panel.add(btnConfigurar);

		JMenuBar menuBar = new JMenuBar();
		menuBar.setBorderPainted(false);
		menuBar.setBounds(0, 0, 1024, 22);
		contentPane.add(menuBar);

		JMenu mnConfiguraes = new JMenu("Configurações");
		menuBar.add(mnConfiguraes);

		JMenuItem mntmConexo = new JMenuItem("Conexão");
		mntmConexo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showConfigureView();
			}
		});
		mnConfiguraes.add(mntmConexo);

		JSeparator separator = new JSeparator();
		mnConfiguraes.add(separator);

		JMenuItem mntmSair = new JMenuItem("Sair");
		mnConfiguraes.add(mntmSair);

	}

	protected void showConfigureView() {

		if (btnConfigurar.isEnabled()) {
			ConfigureServerView awindow = ConfigureServerView.getInstance();
			awindow.setVisible(true);
		}
	}

	@Override
	public void onPreConnect(String messsage) {

		enableDisableButtons(false);

		lblConnectIpAddress.setText(clientContainer.getIpAddress());
		lblConnectPortAddress.setText(String.valueOf(clientContainer.getPort()));
		lblConnectDelay.setText(clientContainer.getDelay() / 1000 + " segundo(s)");
		lblConnectStatus.setText(messsage);
		lblConnectStatus.setForeground(Color.ORANGE);
	}

	@Override
	public void onProgressConnection(String message) {
		lblConnectStatus.setText(message);
		lblConnectStatus.setForeground(Color.BLUE);
	}

	@Override
	public void onLostConnection(String message) {

		enableDisableButtons(true);

		lblConnectStatus.setText(message);
		lblConnectStatus.setForeground(Color.RED);

	}

	@Override
	public void onSuccessfulConnect(String message, Channel channel) {

		enableDisableButtons(true);

		lblConnectStatus.setText(message);
		lblConnectStatus.setForeground(Color.GREEN);

	}

	@Override
	public void onErrorConnection(String message) {

		enableDisableButtons(true);

		lblConnectStatus.setText(message);
		lblConnectStatus.setForeground(Color.RED);

	}

	private void enableDisableButtons(boolean value) {

		btnEnviar.setEnabled(value);
		btnConfigurar.setEnabled(value);

	}
	
	private static String getPath(){
		
		try {
			String path = MainView.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
			if(path.contains(".jar")){
				int index = path.lastIndexOf("/");
				path = path.substring(0, index);
			}
		    System.out.println("Executing at => "+ path);
			return path;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Erro ao tentar localizar caminho da aplicação");
		}
	}
}
