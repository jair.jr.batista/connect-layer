package br.com.fatec.connectlayer.network;

import java.io.IOException;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

public class ClientContainer {

	public static final long DEFAULT_DELAY = 1000;

	private static ClientContainer INSTANCE;
	private final ClientConfigure clientConfigure;

	private ClientContainer() {
		this.clientConfigure = new ClientConfigure();
	}

	public static ClientContainer getInstance() {

		if (INSTANCE == null) {
			INSTANCE = new ClientContainer();
		}

		return INSTANCE;
	}

	public ClientContainer configureIpAddress(final String ipAdress) {
		this.clientConfigure.setIpAdress(ipAdress);
		return this;
	}

	public ClientContainer configurePort(final int port) {
		this.clientConfigure.setPort(port);
		return this;
	}

	public ClientContainer configureDelay(final int delay) {
		this.clientConfigure.setDelay(1000l * delay);
		return this;
	}

	public ClientContainer configureHandler(ChannelHandler channelHandler) {
		this.clientConfigure.setChannelHandler(channelHandler);
		return this;
	}

	public ClientContainer configureConnectionListener(OnConnect listener) {
		this.clientConfigure.setConnectListener(listener);
		return this;
	}

	public void send(String data) throws IOException {

		Channel channel = this.getChannel();

		if (channel != null && channel.isActive()) {

			channel.write(Unpooled.copiedBuffer((data + "\n").getBytes()));
			channel.flush();
		} else {
			throw new IOException("Can't send message to inactive connection");
		}

	}

	public Channel getChannel() {
		Channel channel = this.clientConfigure.getChannel();
		return channel;
	}

	public void start() {
		Channel channel = this.clientConfigure.getChannel();

		if (channel == null || !channel.isActive()) {
			clientConfigure.tryConnect();
		}
	}

	public void close() {
		try {
			Channel channel = this.clientConfigure.getChannel();
			channel.close().sync();

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public boolean isConnected() {
		return this.clientConfigure.isConnected();
	}

	public long getDelay() {

		return this.clientConfigure.getDelay();
	}

	// TODO Separar classe
	private class ClientConfigure {

		private String ipAdress;
		private int port;
		private long delay;
		private Channel channel;
		private ChannelHandler channelHandler;
		private EventLoopGroup group;
		//private Timer timer;
		private int tires = 5;
		private OnConnect connectListener;
		private boolean isConnected = false;

		public ClientConfigure() {
			//this.timer = new Timer();
		}

		public void setIpAdress(String ipAdress) {
			this.ipAdress = ipAdress;
		}

		public void setDelay(long delay) {
			this.delay = delay;
		}

		public String getIpAdress() {
			return ipAdress;
		}

		public int getPort() {
			return port;
		}

		public void setChannelHandler(ChannelHandler channelHandler) {
			this.channelHandler = channelHandler;
		}

		public void setPort(int port) {
			this.port = port;
		}

		private void connectionEstablished() {

			if (connectListener != null) {
				connectListener.onSuccessfulConnect("Conectado", channel);
			}

			tires = 0;
			this.setConnected(true);

		}

		private void connectionLost() {

			if (connectListener != null) {
				connectListener.onLostConnection("Desconectado");
			}

			this.isConnected = false;

		}

//		private void scheduleConnect(long millis) {
//
//			timer.schedule(new TimerTask() {
//				@Override
//				public void run() {
//					tryConnect();
//				}
//			}, millis);
//		}

		public void tryConnect() {

			group = new NioEventLoopGroup();

			Bootstrap bootstrap = new Bootstrap().group(group).channel(NioSocketChannel.class).handler(channelHandler);

			new Thread(new Runnable() {

				@Override
				public void run() {

					if (connectListener != null) {
						connectListener.onPreConnect("Conectando...");
					}

					if (ipAdress == null || ipAdress.equals("")) {

						if (connectListener != null) {
							connectListener.onErrorConnection("O endereço de ip não pode ser nulo ou vazio.");
						}

						throw new RuntimeException("O endereço de ip não pode ser nulo ou vazio.");
					}

					if (port == 0) {

						if (connectListener != null) {
							connectListener.onErrorConnection("Porta inválida, tente configurar outra porta da rede.");
						}

						throw new RuntimeException("Porta inválida, tente configurar outra porta da rede.");
					}

					if (channel == null || !channel.isActive()) {

						if (connectListener != null) {
							connectListener.onProgressConnection("Aguarde...");
						}

						try {

							ChannelFuture channelFuture = bootstrap.connect(ipAdress, port);
							channelFuture.addListener(new ChannelFutureListener() {

								@Override
								public void operationComplete(ChannelFuture future) throws Exception {

									if (!future.isSuccess()) {
										// if is not successful, reconnect
										future.channel().close();
										if (tires <= 5) {
											tires++;
											bootstrap.connect(ipAdress, port).addListener(this);
										} else {
											tires = 0;
											if (connectListener != null) {
												connectListener.onErrorConnection(
														"Servidor não encontrado. Numero de tentativas de conexão excedidas.");
											}
										}
									} else {

										// good, the connection is ok
										channel = future.channel();
										// add a listener to detect the
										// connection lost
										addCloseDetectListener(channel);
										connectionEstablished();

									}
								}

								private void addCloseDetectListener(Channel channel) {
									// if the channel connection is lost, the
									// ChannelFutureListener.operationComplete()
									// will be called
									channel.closeFuture().addListener(new ChannelFutureListener() {
										@Override
										public void operationComplete(ChannelFuture future) throws Exception {
											connectionLost();
											// scheduleConnect(5);
										}

									});

								}

							});

						} catch (Exception e) {
							e.printStackTrace();
							group.shutdownGracefully();
						}
					}
				}
			}).start();

		}

		public Channel getChannel() {
			return this.channel;
		}

		public long getDelay() {

			if (delay <= 0) {
				return DEFAULT_DELAY;
			}

			return this.delay;
		}

		public void setConnectListener(OnConnect onConnectListener) {
			this.connectListener = onConnectListener;
		}

		public boolean isConnected() {
			return isConnected;
		}

		public void setConnected(boolean isConnected) {
			this.isConnected = isConnected;
		}

	}

	public String getIpAddress() {
		return this.clientConfigure.getIpAdress();
	}

	public Integer getPort() {
		return this.clientConfigure.getPort();
	}

}
