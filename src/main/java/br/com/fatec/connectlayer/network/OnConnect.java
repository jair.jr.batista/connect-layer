package br.com.fatec.connectlayer.network;

import io.netty.channel.Channel;

public interface OnConnect {

	void onPreConnect(String messsage);

	void onProgressConnection(String message);

	void onLostConnection(String message);

	void onSuccessfulConnect(String message, Channel channel);

	void onErrorConnection(String message);

}
